package week4.try_catch;

public class Cosmodrome {
    private String name;
    private String country;
    private String city;
    private boolean openDoorsForVisitors = false;
    private boolean openDoorsForStaff = false;
    private boolean openDoorsForPress = false;

    public Cosmodrome(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void makeDoorsOpenedForVisitors(){
        openDoorsForVisitors = true;
        System.out.println("Doors for visitors are open !");
    }

    public void makeDoorsOpedForStaff(){
        openDoorsForStaff = true;
        System.out.println("Doors for staff are open !");
    }

    public void makeDoorsOpenedForPress(){
        openDoorsForPress = true;
        System.out.println("Doors for press is opened");
    }

    public void enterCosmodrome() throws ThereIsNoPath{
        if(openDoorsForVisitors && openDoorsForPress){
            System.out.println("You can go to " + name + " !");
        }else{
            throw new ThereIsNoPath("There is no path to " + name);
        }
    }
}