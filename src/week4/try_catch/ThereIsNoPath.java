package week4.try_catch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class ThereIsNoPath extends Exception {
    public ThereIsNoPath(String userNotification){
        super(userNotification);
        PrintWriter pwoutput = null;
        try{
            FileOutputStream txtFile = new FileOutputStream("cosmodromepath.txt");
            pwoutput = new PrintWriter(txtFile);
            pwoutput.println(userNotification);
            pwoutput.close();
        }catch (IOException e){
            System.out.println("Can not find path file");
        }finally {
            pwoutput.close();
        }
    }
}