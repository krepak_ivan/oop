package week4.try_catch;

public class Main {
    public static void main(String[] args) {
        Cosmodrome baikonur = new Cosmodrome("Baikonur");
        baikonur.makeDoorsOpenedForVisitors();
        baikonur.makeDoorsOpedForStaff();

        try{
            baikonur.enterCosmodrome();
        }catch (ThereIsNoPath e){
            e.printStackTrace();
        }
    }
}