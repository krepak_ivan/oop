package factory;

import java.util.ArrayList;

public abstract class Delivery {
    protected String name;
    protected ArrayList<String> parcel;

    public Delivery(){
        this.parcel = new ArrayList<>();
    }

    private String getName(){
        return name;
    }

    public void receive(){
        System.out.println("I received the parcel !");
    }

    public void unpack(){
        System.out.println("I unpacked the parcel !");
    }

    public void pack(){
        System.out.println("I packed the parcel in a new box !");
    }
}
