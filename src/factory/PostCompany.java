package factory;

public abstract class PostCompany {
    protected abstract Delivery callPostCompany(String type);

    public Delivery usePostCompany(String type){
        Delivery postCompany = callPostCompany(type);

        postCompany.receive();
        postCompany.unpack();
        postCompany.pack();

        return postCompany;
    }

}
