package factory;

public class ShymkentDelivery extends Delivery {
    public ShymkentDelivery(){
        name = "Shymkent";
        parcel.add(name);
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in Shymkent !");
    }
}
