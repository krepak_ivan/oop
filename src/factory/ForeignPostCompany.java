package factory;

public class ForeignPostCompany extends PostCompany {
    public Delivery callPostCompany(String type){
        if(type == "Russia") return new RussiaDelivery();
        if(type == "America") return new AmericaDelivery();
        if(type == "Uzbekistan") return new UzbekistanDelivery();
        if(type == "Kyrgyzstan") return new KyrgyzstanDelivery();
        if(type == "China") return new ChinaDelivery();

        return null;
    }
}
