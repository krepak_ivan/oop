package factory;

public class TarazDelivery extends Delivery {
    public TarazDelivery(){
        name = "Taraz";
        parcel.add(name);
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in Taraz !");
    }
}
