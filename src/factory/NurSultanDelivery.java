package factory;

public class NurSultanDelivery extends Delivery {
    public NurSultanDelivery(){
        name = "Astana city delivery";
        parcel.add("Astana");
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in Astana !");
    }
}
