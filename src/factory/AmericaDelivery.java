package factory;

public class AmericaDelivery extends Delivery {
    public AmericaDelivery(){
        name = "New Your";
        parcel.add(name);
        parcel.add("Connecticut");
        parcel.add("Boston");
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in New York !");
    }
}
