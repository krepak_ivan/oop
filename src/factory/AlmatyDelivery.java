package factory;

public class AlmatyDelivery extends Delivery {
    public AlmatyDelivery(){
        name = "Almaty city delivery";
        parcel.add("Almaty city");
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in Almaty !");
    }
}
