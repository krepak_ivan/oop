package factory;

public class RussiaDelivery extends Delivery {
    public RussiaDelivery(){
        name = "Moscow";
        parcel.add(name);
        parcel.add("Omsk");
    }

    @Override
    public void receive(){
        System.out.println("I received this parcel in Moscow !");
    }
}
