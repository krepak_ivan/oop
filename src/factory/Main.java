package factory;

public class Main {
    public static void main(String[] args) {
        PostCompany almatyDelivery = new KazakhstanPostCompany();
        PostCompany nurSultanDelivery = new KazakhstanPostCompany();
        PostCompany shymkentDelivery = new KazakhstanPostCompany();
        PostCompany tarazDelivery = new KazakhstanPostCompany();

        PostCompany chinaDelivery = new ForeignPostCompany();
        PostCompany americaDelivery = new ForeignPostCompany();

        Delivery delivery_one = almatyDelivery.usePostCompany("Almaty");
        System.out.println();
        Delivery delivery_two = nurSultanDelivery.usePostCompany("Nur-Sultan");
        System.out.println();
        Delivery delivery_three = shymkentDelivery.usePostCompany("Shymkent");
        System.out.println();
        Delivery delivery_four = tarazDelivery.usePostCompany("Taraz");
        System.out.println();

        Delivery foreign_delivery_one = chinaDelivery.usePostCompany("China");
        System.out.println();
        Delivery foreign_delivery_two = americaDelivery.usePostCompany("America");
    }
}
