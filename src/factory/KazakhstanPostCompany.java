package factory;

public class KazakhstanPostCompany extends PostCompany {
    public Delivery callPostCompany(String type){
        if(type == "Almaty") return new AlmatyDelivery();
        if(type == "Nur-Sultan") return new NurSultanDelivery();
        if(type == "Shymkent") return new ShymkentDelivery();
        if(type == "Taraz") return new TarazDelivery();

        return null;
    }
}