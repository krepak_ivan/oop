package week6.dip_example;

public interface Authenticator {
    boolean authenticate(User user);
}
