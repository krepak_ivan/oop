package week6.dip_example;

public class LoginManager {
    Authenticator authenticate;

    public LoginManager(Authenticator authenticate) {
        this.authenticate = authenticate;
    }

    public void login(User user){
        authenticate.authenticate(user);
    }
}
