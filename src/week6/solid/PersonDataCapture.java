package week6.solid;

import java.util.Scanner;

public class PersonDataCapture {
    public static Person capture(){
        Person person = new Person();
        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your first name");
        person.setFirstName(scanner.nextLine());
        System.out.println("What is your last name");
        person.setLastName(scanner.nextLine());

        return person;
    }
}
