package week6.solid;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        StandardMessages.welcomeMessage();

        //PersonDataCapture.capture();
        Person user = new Person();

        if(PersonValidator.isValid(user)){
            StandardMessages.endMessage();
        }

        AccountGenerator.create(user);
        StandardMessages.endMessage();
    }
}
