package week6.solid;

public class PersonValidator {
    public static boolean isValid(Person person){
        if(person.getFirstName().isBlank()) {
            StandardMessages.validationErrorMessage();
            StandardMessages.endMessage();
            return false;
        }
        if(person.getLastName().isBlank()){
            StandardMessages.validationErrorMessage();
            StandardMessages.endMessage();
            return false;
        }
        return true;
    }
}
