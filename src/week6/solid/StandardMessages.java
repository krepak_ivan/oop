package week6.solid;

public class StandardMessages {
    public static void welcomeMessage(){
        System.out.println("Welcome to my app !");
    }

    public static void endMessage(){
        System.out.println("Finished");
    }

    public static void validationErrorMessage(){
        System.out.println("You did not give us a valid first name or surname !");
    }
}