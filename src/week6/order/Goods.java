package week6.order;

public class Goods {
    private String name;
    private int price;

    private static int goodsCounter = 0;

    public Goods(String name, int price) {
        this.name = name;
        this.price = price;
        goodsCounter++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public static int getAmountOfGoods() {
        return goodsCounter;
    }
}