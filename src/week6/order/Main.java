package week6.order;

public class Main {
    public static void main(String[] args) {
        Goods airpods_one = new Goods("AirPods 1", 100);
        Goods airpods_two = new Goods("AirPods 2", 200);
        Goods iphone_x = new Goods("iPhone X", 1000);

        Messages.outputAmountOfGoodsInShop();

        OrderItem orderItem_1 = new OrderItem(airpods_one, 2); //2 * 100 = 200
        OrderItem orderItem_2 = new OrderItem(airpods_two, 3); //3 * 200 = 600
        OrderItem orderItem_3 = new OrderItem(iphone_x, 4); //4 * 1 000 = 4 000
        OrderItem orderItem_4 = new OrderItem(airpods_one, 10); //10 * 100 = 1 000
        //Total price = 5 800

        Order order_1 = new Order();
        order_1.addInArray(orderItem_1);
        order_1.addInArray(orderItem_2);
        order_1.addInArray(orderItem_3);
        order_1.addInArray(orderItem_4);

        Messages.outputAmountOfPurchases();
        Messages.calculateTotalCost();
    }
}