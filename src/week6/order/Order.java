package week6.order;

import java.util.ArrayList;

public class Order {
    ArrayList<OrderItem> arrayList = new ArrayList<>(5);
    private static int orderItemCounter = 0;
    private static int totalPrice = 0;

    public void addInArray(OrderItem order_item){
        arrayList.add(order_item);
        orderItemCounter++;
        totalPrice = totalPrice + order_item.getAmount() * order_item.getGoods().getPrice();
    }

    public static int returnAmountOfObjects(){
        return orderItemCounter;
    }

    public static int returnTotalPrice(){
        return totalPrice;
    }
}