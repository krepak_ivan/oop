package week6.order;

public class Messages {
    public static void outputAmountOfGoodsInShop(){
        System.out.println("There are " + Goods.getAmountOfGoods() + " goods in our shop.");
    }

    public static void outputAmountOfPurchases(){
        System.out.println("There are " + Order.returnAmountOfObjects() + " purchases in one order");
    }

    public static void calculateTotalCost(){
        System.out.println("Total cost is " + Order.returnTotalPrice());
    }
}