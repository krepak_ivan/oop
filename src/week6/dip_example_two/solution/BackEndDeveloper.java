package week6.dip_example_two.solution;

public class BackEndDeveloper implements Developer {
    @Override
    public void develop(){
        writeJava();
    }

    private void writeJava(){
        System.out.println("I know Java !");
    }
}
