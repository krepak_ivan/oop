package week6.dip_example_two.solution;

public class Main {
    public static void main(String[] args) {
        BackEndDeveloper bDeveloper = new BackEndDeveloper();
        FrontEndDeveloper fDeveloper = new FrontEndDeveloper();

        Project project = new Project();

        project.addDeveloper(bDeveloper);
        project.addDeveloper(fDeveloper);

        project.implement();

    }
}
