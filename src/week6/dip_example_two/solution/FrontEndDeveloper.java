package week6.dip_example_two.solution;

public class FrontEndDeveloper implements Developer {
    @Override
    public void develop(){
        writeJavaScript();
    }

    public void writeJavaScript(){
        System.out.println("I know Javascript !");
    }
}
