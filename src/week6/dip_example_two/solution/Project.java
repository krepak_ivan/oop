package week6.dip_example_two.solution;

import java.util.ArrayList;

public class Project {
    ArrayList<Developer> developers = new ArrayList<>();
    void addDeveloper(Developer developer){
        developers.add(developer);
    }

    public void implement(){
        developers.forEach(d->d.develop());
    }
}
