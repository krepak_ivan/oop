package week6.dip_example_two.violation;

public class Project {
    private BackEndDeveloper backEndDeveloper = new BackEndDeveloper();
    private FrontEndDeveloper frontEndDeveloper = new FrontEndDeveloper();

    public void implement(){
        backEndDeveloper.writeJava();
        frontEndDeveloper.writeJavascript();
    }
}
