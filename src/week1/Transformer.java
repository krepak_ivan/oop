package week1;

public class Transformer {
    int position;
    Gun gunInLeftHand;
    Gun gunInRightHand;

    public Transformer(int position){
        this.position = position;
        gunInRightHand = new Gun(18);
        gunInLeftHand = new Gun(30);
    }

    public void run(){
        this.position = this.position + 1;
        //we can write position, not this.position
    }
}
