package week2.game;

public class Knight extends Character {
    @Override
    public void fight() {
        System.out.println("Knight is here");
        this.weapon.useWeapon();
    }

    public Knight() {
        weapon = new SwordBehavior();
    }
}
