package week2.game;

public class Troll extends Character {
    public Troll() {
        weapon = new SwordBehavior();
    }

    @Override
    public void fight() {
        System.out.println("Troll is here");
        this.weapon.useWeapon();
    }
}
