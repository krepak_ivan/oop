package week2.game;

public interface WeaponBehavior {
    void useWeapon();
}