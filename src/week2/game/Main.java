package week2.game;

public class Main {
    public static void main(String[] args) {
        Queen queen_1 = new Queen();
        King king_1 = new King();
        Troll troll_1 = new Troll();
        Knight knight_1 = new Knight();

        queen_1.fight();
        king_1.fight();
        troll_1.fight();
        knight_1.fight();

        queen_1.setWeapon(new AxeBehavior());
        queen_1.fight();
        king_1.setWeapon(new BowAndArrowBehavior());
        king_1.fight();
        troll_1.setWeapon(new KnifeBehavior());
        troll_1.fight();
        knight_1.setWeapon(new AxeBehavior());
        knight_1.fight();
    }
}
