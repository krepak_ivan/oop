package week2.game;

public class King extends Character {
    public King() {
        weapon = new AxeBehavior();
    }

    @Override
    public void fight() {
        System.out.println("King is here");
        this.weapon.useWeapon();
    }
}
