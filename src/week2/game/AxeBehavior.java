package week2.game;

public class AxeBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("My weapon is axe");
    }
}
