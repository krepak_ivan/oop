package week2.game;

public class BowAndArrowBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("My weapon is bow and arrows");
    }
}
