package week2.game;

public class Queen extends Character {
    public Queen() {
        weapon = new BowAndArrowBehavior();
    }

    @Override
    public void fight() {
        System.out.println("Queen is here");
        this.weapon.useWeapon();
    }
}
