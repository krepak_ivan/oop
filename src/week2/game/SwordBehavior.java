package week2.game;

public class SwordBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("My weapon is sword");
    }
}
