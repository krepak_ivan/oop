package week2.read_from_txt;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        String myText = "";
        char lf = 0x0A;
        String endLine = ""+lf;
        try (FileInputStream myFile = new FileInputStream("src\\week2\\read_from_txt\\1.txt");
             InputStreamReader inputStreamReader = new InputStreamReader(myFile, "UTF-8");
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String nextLine;
            boolean eof = false;
            while (!eof) {
                nextLine = reader.readLine();
                if (nextLine == null){
                    eof = true;
                } else {
                    myText += nextLine+endLine;
                    // myTextArray.add(nextLine);
                }
            }
        }catch (IOException e){
            System.out.println("Can't read Stalking.txt");
        }
        System.out.println(myText);
    }
}
