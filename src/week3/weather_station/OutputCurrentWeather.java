package week3.weather_station;

public class OutputCurrentWeather implements Observer, Output {
    private float temperature;
    private float pressure;
    private float humidity;
    private Subject weatherData;

    public OutputCurrentWeather(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    @Override
    public void updateData(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    @Override
    public void output() {
        System.out.println("Current weather is : " + temperature + "*C, " + pressure + "Pa, " + humidity + "%");
    }

}