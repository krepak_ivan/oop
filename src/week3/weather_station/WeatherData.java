package week3.weather_station;

import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject {
    private List<Observer> allObservers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){
        allObservers = new ArrayList<>();
    }

    public void setWeatherMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    @Override
    public void addObserver(Observer observer) {
        allObservers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        int indexObserver = allObservers.indexOf(observer);
        if(indexObserver >= 0){
            allObservers.remove(observer);
        }
    }

    @Override
    public void notifyAllObservers() {
        for(Observer arrayElement: allObservers){
            arrayElement.updateData(temperature, humidity, pressure);
        }
    }

    public void measurementsChanged() {
        notifyAllObservers();
    }
}