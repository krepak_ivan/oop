package week3.weather_station;

public interface Observer {
    void updateData(float temperature, float humidity, float pressure);
}