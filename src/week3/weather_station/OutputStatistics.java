package week3.weather_station;

public class OutputStatistics implements Observer, Output {
    public Subject weatherData;
    int counter = 0;
    float minimalTemperature = 100;
    float maximalTemperature = 0f;
    float sumTemperature = 0f;
    float minimalPressure = 100;
    float maximalPressure = 0f;
    float sumPressure = 0f;
    float minimalHumidity = 100;
    float maximalHumidity = 0f;
    float sumHumidity = 0f;

    public OutputStatistics(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    @Override
    public void updateData(float temperature, float humidity, float pressure) {
        if(maximalTemperature < temperature) maximalTemperature = temperature;
        if(minimalTemperature > temperature) minimalTemperature = temperature;
        sumTemperature = sumTemperature + temperature;
        if(maximalPressure < pressure) maximalPressure = pressure;
        if(minimalPressure > pressure) minimalPressure = pressure;
        sumPressure = sumPressure + pressure;
        if(maximalHumidity < humidity) maximalHumidity = humidity;
        if(minimalHumidity > humidity) minimalHumidity = humidity;
        sumHumidity = sumHumidity + humidity;

        counter++;
        output();
    }

    @Override
    public void output() {
        System.out.println("Average temperature is : " + sumTemperature / counter);
        System.out.println("Average pressure is : " + sumPressure / counter);
        System.out.println("Average humidity is : " + sumHumidity / counter);

        System.out.println("Maximal temperature is : " + maximalTemperature + ", minimal temperature is : " +
                minimalTemperature);
        System.out.println("Maximal pressure is : " + minimalPressure + ", minimal pressure is : " +
                minimalPressure);
        System.out.println("Maximal humidity is : " + maximalHumidity + ", minimal humidity is : " +
                minimalHumidity);
    }
}