package week3.weather_station;

public class Main {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        OutputCurrentWeather outputCurrentWeather =  new OutputCurrentWeather(weatherData);
        OutputForecast outputForecast = new OutputForecast(weatherData);
        OutputStatistics outputStatistics = new OutputStatistics(weatherData);

        weatherData.setWeatherMeasurements(80, 65, 30.4f);
        weatherData.setWeatherMeasurements(82, 70, 29.2f);
        weatherData.setWeatherMeasurements(78, 90, 29.2f);
    }
}