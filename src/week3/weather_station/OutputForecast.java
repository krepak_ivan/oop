package week3.weather_station;

public class OutputForecast implements Observer, Output {
    private float lastTemperature;
    private float currentTemperature = 29;
    private float lastHumidity;
    private float currentHumidity = 29;
    private float lastPressure;
    private float currentPressure = 29;
    private Subject weatherData;

    public OutputForecast(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    @Override
    public void updateData(float temperature, float humidity, float pressure) {
        lastTemperature = currentTemperature;
        currentTemperature = temperature;
        lastPressure = currentPressure;
        currentPressure = pressure;
        lastHumidity = currentHumidity;
        currentHumidity = humidity;
        output();
    }

    @Override
    public void output() {
        System.out.print("Forecast is : ");

        if(currentTemperature > lastTemperature || currentPressure > lastPressure || currentHumidity > lastHumidity){
            System.out.print("Now weather is better");
            System.out.println();
        }else if (currentTemperature < lastTemperature || currentPressure < lastPressure ||
        currentHumidity < lastHumidity){
            System.out.print("Now weather is worse");
            System.out.println();
        }else{
            System.out.print("Weather is almost the same");
            System.out.println();
        }
    }
}